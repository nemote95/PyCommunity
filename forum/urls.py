#imports(
from django.conf.urls import *
from django.contrib.auth.decorators import login_required as LR
from models import *
from views import *
#)
#==========================================================================================
urlpatterns = patterns('',
    url(r"^forum/(?P<dpk>\d+)/$", ForumView.as_view(), name= "forum"),
    url(r"^topic/(?P<dpk>\d+)/$", TopicView.as_view(), name= "topic"),
    url(r"^new_topic/(?P<dpk>\d+)/$"  , LR(NewTopic.as_view()), name= "new_topic"),
    url(r"^reply/(?P<dpk>\d+)/$" , LR(Reply.as_view()), name="reply"),
    url(r"^profile/(?P<pk>\d+)/$"  , LR(EditProfile.as_view()), name="profile"),
    url(r"^viewprofile/(?P<dpk>\d+)/$" , LR(UserPageView.as_view()), name="viewprofile"),
    url(r"^new_wallpost/(?P<dpk>\d+)/$"  , LR(NewWallpost.as_view()), name= "new_wallpost"),
    url(r"^(?P<pk>\d+)/like/$", LR(like), name='like'),
    url(r"^(?P<pk>\d+)/friendlist/$", LR(friendsList), name="friendlist"),
    url(r"^(?P<pk>\d+)/addfriend/$", LR(friendAdd),name="addfriend"),
    url(r"^(?P<pk>\d+)/removefriend/$", LR(friendRemove),name="removefriend"),
    url(r"^$", Main.as_view(), name="main"),
)
