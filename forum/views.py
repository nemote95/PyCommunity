# imports (
from models import *
from utils import *
from django.views.generic.edit import  UpdateView as gUpdateView
from django.shortcuts import get_object_or_404
from mcbv.detail import DetailView
from mcbv.edit import CreateView, UpdateView
from mcbv.list_custom import ListView, ListRelated,DetailListCreateView
from forms import PostForm , WallpostForm
# )
#=========================================================================================
class Main(ListView):
    """desplays list of forums names and their last post."""
    list_model    = Forum
    template_name = "list.html"
#=========================================================================================
class ForumView(ListRelated):#(listview+detailview)
    """desplays all of the topics in each forum"""
    detail_model  = Forum
    list_model    = Topic
    related_name  = "topics"
    template_name = "forum.html"
#=========================================================================================
class TopicView(ListRelated):
    """desplays all of the posts in each topic"""
    list_model    = Post
    detail_model  = Topic
    related_name  = "posts"
    template_name = "topic.html"
#=========================================================================================
class UserPageView(ListRelated):
    """ desplays the fields of user's profile and all of the wallposts"""
    list_model    = Wallpost
    detail_model  = UserProfile
    related_name  = "wallposts"
    template_name = "userpage.html"
#=========================================================================================
class EditProfile(gUpdateView):
    """to update the profile fields by this view """
    model = UserProfile
    fields = ['name','country','avatar','status', 'personalDetails']
    template_name_suffix = '_update_form'
#=========================================================================================
class NewWallpost(DetailView, CreateView):
    
    detail_model = UserProfile
    form_model = Wallpost
    modelform_class = WallpostForm
    body = "New Wallpost"
    template_name = "wallpost.html"

    def get_profile(self, modelform):
        """ gives the current profile object"""
        return self.get_detail_object()

    def modelform_valid(self, modelform):
        """ creates a new wallpost on a profile page """
        body = modelform.cleaned_data['body']
        profile = self.get_profile(modelform)
        Wallpost.obj.create(profile=profile,body=body, creator=self.user)
        return redir(self.get_success_url())

    def get_success_url(self):
        return self.get_detail_object().get_absolute_url()
#=========================================================================================
class NewTopic(DetailView, CreateView):
    detail_model = Forum
    form_model = Post
    modelform_class = PostForm
    title = "Start New Topic"
    template_name = "post.html"

    def get_topic(self, modelform):
        """ creates a topic in the current forum"""
        title = modelform.cleaned_data.title
        return Topic.obj.create(forum=self.get_detail_object(), title=title, creator=self.user)

    def modelform_valid(self, modelform):
        """creates first post of the new topic """
        data   = modelform.cleaned_data
        topic = self.get_topic(modelform)
        Post.obj.create(topic=topic, title=data.title, body=data.body, creator=self.user)
        self.user.profile.incrementPosts()
        return redir(self.get_success_url())

    def get_success_url(self):
        return self.get_detail_object().get_absolute_url()

#=========================================================================================
class Reply(NewTopic):

    detail_model = Topic
    title = "Reply"
    
    def get_topic(self, modelform):
        """gives the current topic class to save a reply in it"""
        return self.get_detail_object()

    def get_success_url(self):
        """brings back the user to current topic page"""
        return self.get_detail_object().get_absolute_url() + "?page=last"
#=========================================================================================
def like(request, pk):
    """ when a user press the like buttons the number of post creator's score will be increased
        and an email will be sent to postcreator"""
    post = get_object_or_404(Post, pk=pk)
    topic=post.topic
    user=request.user
    
    post.creator.profile.incrementScores()
    
    message=  "%s (%s) Liked  your post :%s(%s)"% (user,'http://localhost:8000/forum/viewprofile/%d/'%(user.id), post,'http://localhost:8000/forum/topic/%d/'%(topic.id))
                                
    recipient_list = [post.creator.email]
    from_addr="pycommunity.noreply@gmail.com"
    send_mail("Like Notification", message, from_addr, recipient_list)
    
    return HttpResponseRedirect(reverse('forum:topic', args=(topic.id,)))
#==========================================================================================
def friendsList(request, pk):
    """ shows the list of user's page . """
    user = get_object_or_404(User, pk=pk)
    friends = [friendship.toFriend for friendship in user.friendSet.all() if friendship.submitted()]
    
    variables = RequestContext(request,{ 'friends': friends})
    return render_to_response('friendsListPage.html', variables)
#==========================================================================================
def friendAdd(request,pk):
    """this function  saves a side of a friendship and email to friend,if friend adds the user too,
        their friendship will be submitted
        and bring back the user to the friend's profile page"""
    user=request.user
    friend = get_object_or_404(User,pk=pk)
    
    friendship = Friendship(fromFriend=user,toFriend=friend)
    friendship.save()
    
    if friendship.submitted():
        message        =  "friendship with %s(%s) has been submitted."% (user,'http://localhost:8000/forum/viewprofile/%d/'%(user.id))
    else:
        message =  "%s (%s) added you to her(him) friend list,if you want to submit this friendship add her(him) to your friends list. "% (user,'http://localhost:8000/forum/viewprofile/%d/'%(user.id))
                                
    from_addr  ="pycommunity.noreply@gmail.com"
    recipient_list = [friend.email]
    send_mail("Friendship Notification", message, from_addr, recipient_list)
    
    return HttpResponseRedirect(reverse('forum:viewprofile' ,args=(friend.pk,)))
#==========================================================================================
def friendRemove(request,pk):
    """this function  removes the both sides of friendship
        and bring back the user to the friend's profile page"""
    user=request.user
    friend = get_object_or_404(User,pk=pk)
    
    friendship1 = Friendship.objects.get(fromFriend=request.user, toFriend=friend)
    friendship1.delete()
    
    friendship2 = Friendship.objects.get(fromFriend=friend, toFriend=request.user)
    friendship2.delete()
    
    return HttpResponseRedirect(reverse('forum:viewprofile' ,args=(friend.pk,)))
