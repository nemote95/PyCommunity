#imports(
from django.core.mail import send_mail
from django.db.models import *
from django.contrib.auth.models import User
from django.contrib import admin
from django.db.models.signals import post_save
from  utils import *
#)
#==========================================================================================
class UserProfile(BaseModel):
    avatar = URLField(max_length=200)
    posts  = IntegerField(default=0)
    user   = OneToOneField(User, related_name="profile")
    name=CharField(max_length=60)
    country=CharField(max_length=60)
    scores=IntegerField(default=0)
    status=TextField(max_length=500)
    personalDetails= TextField(max_length=1000)
    

    def __unicode__(self):
        return unicode(self.user)

    def get_absolute_url(self) :
        """it gets the absolute url of current user page , this method has been used in views"""
        return reverse("forum:viewprofile",args=[self.pk])

    def incrementPosts(self):
        """this method will be used for each new post to increment the number of user's posts"""
        self.posts += 1
        self.save()
        
    def incrementScores(self):
        """this method will be used for each new post to increment the number of user's post"""
        self.scores += 1
        self.save()
        
#==========================================================================================

class Forum(BaseModel):
    
    title = CharField(max_length=60)
    
    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
       """ it gets the absolute url of current forum page , this method has been used in views"""
       return reverse("forum:forum",args=[self.pk])

    def numPosts(self):
        """returns the total number of  the posts in each forum"""
        return sum([t.numPosts() for t in self.topics.all()])

    def lastPost(self):
        """finds the last post of all the topics in a forum"""
        topics = self.topics.all()# the list of topics in a forum
        last    = None
        for topic in topics:#like finding the maximum of a list
            templastp = topic.lastPost()
            if templastp and (not last or templastp.date > last.date):
                last = templastp
        return last

#==========================================================================================
class Topic(BaseModel):
    title   = CharField(max_length=60)
    date = DateTimeField(auto_now_add=True)
    creator = ForeignKey(User, blank=True, null=True)
    forum   = ForeignKey(Forum, related_name="topics")

    class Meta:
        ordering = ["-date"]# order topics by their date of creation

    def __unicode__(self):
        return unicode("%s - %s" % (self.creator, self.title))

    def get_absolute_url(self) :
        """ it gets the absolute url of current topic page, this method has been used in views"""
        return reverse("forum:topic", args=[self.pk])
    
    def lastPost(self)  :
        """returns the most recent post of the topic"""
        return first(self.posts.all())#the first post is the recent post
    
    def numPosts(self) :
        """returns the number of  the posts in each topic"""
        return self.posts.count()
    
    def numReplies(self) :
        """returns the number of the replies in each topic"""
        return self.posts.count() - 1#because the first post isn't a reply
    
#==========================================================================================
class Post(BaseModel):
    title = CharField(max_length=60)
    date = DateTimeField(auto_now_add=True)
    creator=ForeignKey(User, blank=True, null=True)
    topic = ForeignKey(Topic, related_name="posts")
    body = TextField(max_length=10000)

    class Meta:
        ordering = ["date"]

    def __unicode__(self):
        return u"%s - %s - %s" % (self.creator, self.topic, self.title)

    def short(self):
        """this method is used in lastposts column to show creator and title and date"""
        date = self.date.strftime("%b %d, %I:%M %p")
        return u"%s - %s\n%s" % (self.creator, self.title, date)

    def profileData(self):
        """this method is used in each post to see the creator's avatar and number of his posts"""
        p = self.creator.profile
        return p.posts, p.avatar
#===========================================================================================
class Wallpost(BaseModel):
    creator = ForeignKey(User, blank=True, null=True)
    body = TextField(max_length=1000)
    profile = ForeignKey(UserProfile, related_name="wallposts",  blank=True, null=True)
    date = DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ["date"]    
    def __unicode__(self):
        return u"%s: %s" % (self.profile, self.body[:60])

    def save(self, *args, **kwargs):
        if self.creator!=self.profile.user:
            """email when a wallpost is added."""
            message        = "Wallpost  was added to your profile page  by '%s':\n%s" % ( self.creator, self.body)
            from_addr      = "pycommunity.noreply@gmail.com"
            recipient_list = [self.profile.user.email]
            send_mail("New Wallpost Added", message, from_addr, recipient_list)
        super(Wallpost, self).save(*args, **kwargs)
#==========================================================================================
class Friendship(Model):
    fromFriend = ForeignKey(User, related_name='friendSet')
    toFriend = ForeignKey(User, related_name='toFriendSet')
    
    def submitted(self):
        """"the friendship is submitted if it has tow sides."""
        if Friendship.objects.filter(fromFriend=self.toFriend,toFriend=self.fromFriend):
            return True
        return False
    def __unicode__(self):
        return u'%s, %s' % (self.fromFriend.username,self.toFriend.username)
    
    class Meta:
        """the friendship sides must be unique"""
        unique_together = (('toFriend', 'fromFriend'), )
        
