#imports(
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from models import *
#)
#==========================================================================================
class SimpleTest(TestCase):
    def setUp(self):
        forum  = Forum.objects.create(title="forum")
        user   = User.objects.create_user("negar", "nemote95@gmail.com", "pwd")
        topic = Topic.objects.create(title="topic", creator=user, forum=forum)
        UserProfile.objects.create(user=user)
        Site.objects.create(domain="test.org", name="test.org")
        Post.objects.create(title="post", body="body", creator=user, topic=topic)

    def content_test(self, url, values):
        """get content of url and test that each of items in `values` list is present."""
        r = self.c.get(url)
        self.assertEquals(r.status_code, 200)
        for v in values:
            self.assertTrue(v in r.content)

    def test(self):
        self.c = Client()
        self.c.login(username="negar", password="pwd")
        # test forum listing, topic listing and post page
        self.content_test("/forum/", ['<a href="/forum/forum/1/">forum</a>'])
        self.content_test("/forum/forum/1/", ['<a href="/forum/topic/1/">topic</a>', "negar - post"])
        self.content_test("/forum/topic/1/",['<div class="ttitle">topic</div>', '<span class="title">post</span>', 'body <br />', 'by negar |'])
        # test creation of a new topic and a reply
        r = self.c.post("/forum/new_topic/1/", {"title": "topic2", "body": "body2"})
        r = self.c.post("/forum/reply/2/", {"title": "post2", "body": "body3"})
        self.content_test("/forum/topic/2/",['<div class="ttitle">topic2</div>','<span class="title">post2</span>','body2 <br />', 'body3 <br />'])
