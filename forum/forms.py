#imports(
from django.forms import ModelForm
from models import *
from utils import ContainerFormMixin
#)
#====================================================================================
class PostForm(ContainerFormMixin, ModelForm):
    """post form class which contains all the fields except creator and topic fields."""
    class Meta:
        model   = Post
        exclude = ["creator", "topic"]
#=====================================================================================        
class WallpostForm(ModelForm):
    """wallpost form which contains all the fields except creator and profile page fields."""
    class Meta:
        model = Wallpost
        exclude = ["profile","creator"]
