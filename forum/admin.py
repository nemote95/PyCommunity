#imports:
from django.contrib import admin
from forum.models import *
#)

#==========================================================================================
class ProfileAdmin(admin.ModelAdmin):
    list_display = ["user","scores"]

class TopicAdmin(admin.ModelAdmin):
    list_display = ["title", "forum", "creator" ,"date"]
    
class PostAdmin(admin.ModelAdmin):
    search_fields = ["title"]
    list_display  = ["title","topic","creator","date"]

class WallpostAdmin(admin.ModelAdmin):
    list_display=["creator","profile","date"]
    
class FriendshipAdmin(admin.ModelAdmin):
    list_display=["fromFriend","toFriend"]
    
def createUserProfile(sender, **kwargs):
    """this function will make a profile for a new user"""
    user = kwargs["instance"]
    if not UserProfile.objects.filter(user=user):
        UserProfile(user=user).save()
        
post_save.connect(createUserProfile, sender=User)
#==========================================================================================
admin.site.register(Forum)
admin.site.register(Topic, TopicAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(UserProfile, ProfileAdmin)
admin.site.register(Wallpost, WallpostAdmin)
admin.site.register(Friendship, FriendshipAdmin)

