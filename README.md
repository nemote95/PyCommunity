PyCommunity
===========

PyCommunit version 1.0  4/21/2014

this is a simple community app written in Python.(AP course project: Computer Department of Iran University Of Science and Technology)

MODULE REQUIREMENTS:
django1.6  , mbvc(https://pypi.python.org/pypi/django-mcbv/0.4) , django registration(https://pypi.python.org/pypi/django-registration/1.0)

HOW TO RUN:
settings.py > change Email address and password
manage.py syncdb
manag.py runserver

FEATURES:
this app includes forums which let users create new topics and posts.
users can edit their profiles and create new wallpost on their profile and their friend's profile.
users can add friends and remove them too.

AUTHOR:
Negar Mohamadhasan
nemote95@gmail.com

